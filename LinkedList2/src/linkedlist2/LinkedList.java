

package linkedlist2;

public class LinkedList {
    
    private Node first;
    
    public LinkedList(Node n) {
        first = n;
    }
    
    public void addNode(Node n){
        Node temp = first;
        while(temp.getNext() != null){
            temp = temp.getNext();
        }
        temp.setNext(n);
    }
    
    public void printLinkedList(){
        Node temp = first;
        while(temp != null){
            System.out.print(temp.getValue());
            System.out.print(", ");
            temp = temp.getNext();
        }
        System.out.println("");
    }
    
    public void deleteNode(Node n){
        Node temp = first;
        Node previous = null;
        while(temp != null){
            if(n.getValue()== temp.getValue()){
                previous.setNext(temp.getNext());
            }
            previous = temp;
            temp = temp.getNext();
        }
    }
    
    public void addBeginning(Node n){
        n.setNext(first);
        first = n;
    }
    
    public void deleteNodePlace(int a){
        if(a == 0){
            first = first.getNext();
        }else{
            
        Node temp = first;
        Node previous = null;
        for(int i = 1; i<=a; i++){
            previous = temp;
            temp = temp.getNext();
        }
        previous.setNext(temp.getNext());
        }
    }
    
    public void addNodePlace(int a, Node n){
        
        Node temp = first;
        Node previous = null;
        if(a>nodeSize())
            addNode(n);
        else{
            if(a == 1)
                addBeginning(n);
            else{
                for (int i = 1; i < a; i++) {
                    previous = temp;
                    temp = temp.getNext();
                }
            previous.setNext(n);
            n.setNext(temp);    
            }
        }
    }
        
    public int nodeSize(){
        int length = 0;
        Node temp = first;
        while(temp != null){
            temp = temp.getNext();
            length++;
        }
        return length;
    }
    
    public Node stelleX(int a){
        Node temp = first;
        int count = 0;
        while(temp != null){
            if(count == a)
                return temp;
            temp = temp.getNext();
            count++;
        }
        return temp;
    }
    
    public void sort(){
        for(int i=nodeSize()-1; i>0; i--){
            for(int j=0; j < nodeSize()-1; j++ ){
                if(stelleX(j).getValue() > stelleX(j+1).getValue()){
                    int temp = stelleX(j+1).getValue();
                    stelleX(j+1).setValue(stelleX(j).getValue());
                    stelleX(j).setValue(temp);
                }
            
            }
        
        }
    }

            

    public static void main(String[]args){
        Node strat = new Node(888);
        Node n1 = new Node(666);
        Node n2 = new Node(444);
        Node n3 = new Node(222);
        Node n4 = new Node(100);
        Node n5 = new Node(5555);
        
        LinkedList ll = new LinkedList(strat);
        ll.addNode(n1);
        ll.addNode(n2);
        ll.addNode(n3);
        ll.addNode(n4);
        
        ll.addNodePlace(20, n5);
        //ll.deleteNodePlace(0);
        ll.printLinkedList();
        ll.sort();
        ll.printLinkedList();
        System.out.println(ll.nodeSize());
    
   
   
    }
    
}